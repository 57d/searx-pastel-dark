# SearX Pastel Dark css file.

- [Oscar theme](#oscar)
- [Simple theme](#simple)

## Oscar
**To work properly install oscar Logicodev (not dark) theme in searx preferences**

<img src="/Oscar Images/main.png" alt="Main" width="80%"/>

<img src="/Oscar Images/preferences.png" alt="Preferences" width="80%"/>

<img src="/Oscar Images/search.png" alt="Search Example" width="80%"/>

**All images are available [here](/Oscar Images/)**

## Simple
**To work properly install simple dark theme in searx preferences**

<img src="/Simple Images/main.png" alt="Main" width="80%"/>

<img src="/Simple Images/preferences.png" alt="Preferences" width="80%"/>

<img src="/Simple Images/search.png" alt="Search Example" width="80%"/>

**All images are available [here](/Simple Images/)**


## If you find any issue, please, report it [here](https://gitlab.com/57d/searx-pastel-dark/-/issues/new)
